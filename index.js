/* ამოცანა #1
Make a program that filters a list of strings and returns a list with only your friends name in it. If a name has exactly 4 letters in it, you can be sure that it has to be a friend of yours! Otherwise, you can be sure he's not...
Ex: Input = ["Ryan", "Kieran", "Jason", "Yous"], Output = ["Ryan", "Yous"]

friend ["Ryan", "Kieran", "Mark"] shouldBe ["Ryan", "Mark"]
ტესტ ქეისები (ამ მასივებზე შეგიძლიათ გატესტოთ ფუნქცია)
["George", "Nick", "Tom", "Kate", "Annie"] უნდა დააბრუნოს ["Nick", "Kate"]
["James", "Will", "Jack", "Nate", "Edward"] უნდა დააბრუნოს ["Will", "Jack", "Nate"] */

const guys = ["Rick", "Morty", "Konstantine", "Luka", "Jemali", "Jumberi"];

const friends = guys.filter(friend => friend.length == 4);
console.log(friends);

/* ამოცანა #2
Create a function that returns the sum of the two lowest positive numbers given an array of minimum 4 positive integers. No floats or non-positive integers will be passed.
For example, when an array is passed like [19, 5, 42, 2, 77], the output should be 7.
[10, 343445353, 3453445, 3453545353453] should return 3453455.

ტესტ ქეისები (ამ მასივებზე შეგიძლიათ გატესტოთ ფუნქცია)
[5, 8, 12, 19, 22] უნდა დააბრუნოს 5+8 ის ჯამი (13)
[52, 76, 14, 12, 4] უნდა დააბრუნოს 4 + 12 ის ჯამი (16)
[3, 87, 45, 12, 7] უნდა დააბრუნოს 3 + 7 ის ჯამი (10) */

const array = [29, 3, 34, 17, 8, 85, 103];
const anotherArray = [5, 8, 12, 19, 22];
const differentArray = [52, 76, 14, 12, 4];

function sum(numbers) {
  const arr = numbers.sort(function (a, b) {
    return a - b;
  });
  const result = arr[0] + arr[1];
  return result;
}

console.log(sum(array));
console.log(sum(anotherArray));
console.log(sum(differentArray));
